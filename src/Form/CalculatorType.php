<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Valid;

class CalculatorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('first_input', TextType::class, [
                'label' => 'First Operand',
                'constraints' => [
                    new Type('numeric',
                          "First Operand value is not valid"
                    ),
                    new NotBlank([
                        'message' =>  "First Operand value should not be blank."
                    ])
                ]
            ])
            ->add('operator', ChoiceType::class, [
                'label' => 'Select Operator',
                'placeholder' => 'Select',
                'choices' => [
                    '👽' => '+',
                    '💀' => '-',
                    '👻' => '*',
                    '😱' => '/'
                ],
                'attr' =>[
                    'class' => 'dropDown1',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' =>  "Please select Operator for your calculation"
                    ])
                ]
            ])
            ->add('second_input', TextType::class, [
                'label' => 'Second Operand',
                'constraints' => [
                    new Type('numeric',
                        "Second Operand value is not valid"
                    ),
                    new NotBlank([
                        'message' =>  "Second Operand value should not be blank."
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
