<?php

namespace App\Services\Calculators;

class MultiplicationCalculator implements CalculatorInterface
{
    /**
     * @param string $method
     * @return bool
     */
    public function supports($method): bool
    {
        return $method === '*';
    }

    /**
     * @param int|float $value1
     * @param int|float $value2
     * @return float|int
     */
    public function getResult($value1, $value2)
    {
        return $value1 * $value2;
    }
}
