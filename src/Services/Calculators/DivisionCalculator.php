<?php

namespace App\Services\Calculators;

use Exception;
use InvalidArgumentException;

class DivisionCalculator implements CalculatorInterface
{
    /**
     * @param string $method
     * @return bool
     */
    public function supports($method): bool
    {
        return $method === '/';
    }

    /**
     * @param int|float $value1
     * @param int|float $value2
     * @return float|int|string
     */
    public function getResult($value1, $value2)
    {
        try {
            if ($value2 == 0) {
                throw new InvalidArgumentException("Second operand can't be zero!", 400);
            }

            return $value1 / $value2;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
