<?php

namespace App\Services\Calculators;

interface CalculatorInterface
{
    public function supports($method): bool;
    public function getResult($value1, $value2);
}