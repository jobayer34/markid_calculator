<?php

namespace App\Controller;

use App\Form\CalculatorType;
use App\Services\Calculators\DivisionCalculator;
use App\Services\Calculators\MultiplicationCalculator;
use App\Services\Calculators\SubtractionCalculator;
use App\Services\Calculators\SummationCalculator;
use App\Services\ErrorManagerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route ("/", name="app_home")
     */
    public function appHome(Request $request, ErrorManagerService $errorManagerService): Response
    {
        $calculatorForm = $this->createForm(CalculatorType::class);
        $calculatorForm->handleRequest($request);
        $result['status'] = 0;

        if ($calculatorForm->isSubmitted() && $calculatorForm->isValid()) {
            $formData = $calculatorForm->getData();

            $calculators = [
                new SummationCalculator(),
                new SubtractionCalculator(),
                new DivisionCalculator(),
                new MultiplicationCalculator()
            ];

            foreach ($calculators as $calculator) {
                if ($calculator->supports($formData['operator'])) {
                    $result['output'] = $calculator->getResult($formData['first_input'], $formData['second_input']);
                    break;
                }
            }

            $result['status'] = 1;
            return new JsonResponse($result, 200);
        } else {
            $errors = $errorManagerService->getErrorsFromForm($calculatorForm);
        }

        /* error management */
        if (isset($errors['first_input']) || isset($errors['second_input']) || isset($errors['operator'])) {
            $result['errors'] = $errors;
            return new JsonResponse($result, 200);
        }

        return $this->render('home.html.twig', [
            'calculatorForm' => $calculatorForm->createView()
        ]);
    }
}
