### Installation

Please follow these steps to install this project

1. Clone the project
   ```sh
   git clone https://jobayer34@bitbucket.org/jobayer34/markid_calculator.git
   ```
2. Move to project folder
   ```sh
   cd markid_calculator
   ```
3. Install project dependencies
   ```sh
   composer install
   ```
4. Install frontend packages
   ```sh
   yarn
   ```
5. Install assets
   ```sh
   yarn encore prod
   ```
6. If you are using symfony server, then run
   ```sh
   symfony serve --port='your preferred port number'
   ```
7. Run below command to run tests cases (optional)
   ```sh
   php ./vendor/bin/phpunit
   ```

