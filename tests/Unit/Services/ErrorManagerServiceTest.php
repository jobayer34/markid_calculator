<?php

declare(strict_types=1);

namespace App\Tests\Unit\Services;

use App\Services\ErrorManagerService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class ErrorManagerServiceTest extends TestCase
{
    /**
     * @var ErrorManagerService
     */
    private $errorManagerService;

    public function setUp(): void
    {
        $this->errorManagerService = new ErrorManagerService();
    }

    /**
     * @param string $expectedValue
     * @dataProvider specificFieldErrorDataProvider
     */
    public function testGetSpecificFieldError(string $expectedValue)
    {
        $form = $this->getMockBuilder(Form::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $formError = $this->getMockBuilder(FormError::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $form->method('getErrors')->willReturn([$formError]);
        $formError->method('getMessage')->willReturn($expectedValue);

        $result = $this->errorManagerService->getSpecificFieldError($form);
        $this->assertSame($expectedValue, $result[0]);
    }

    public function specificFieldErrorDataProvider(): array
    {
        return [
            ["First Operand value is not valid"],
            ["Please select Operator for your calculation"],
            ["Second Operand value is not valid"],
        ];
    }

    /**
     * @param string $inputName
     * @param string $errorMessage
     * @dataProvider errorFromFormProvider
     */
    public function testGetErrorsFromForm(string $inputName, string $errorMessage)
    {
        $formList = $this->getMockBuilder(FormInterface::class)
            ->getMock()
        ;

        $form = $this->getMockBuilder(Form::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $formError = $this->getMockBuilder(FormError::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $form->method('getErrors')->willReturn([$formError]);
        $form->method('getName')->willReturn($inputName);
        $formError->method('getMessage')->willReturn($errorMessage);

        $formList->method('all')->willReturn([$form]);

        $result = $this->errorManagerService->getErrorsFromForm($formList);
        $this->assertArrayHasKey($inputName, $result);
        $this->assertCount(1, $result);
        $this->assertSame($errorMessage, $result[$inputName][0]);
    }

    public function errorFromFormProvider(): array
    {
        return [
            ['first_input', 'First Operand value is not valid'],
            ['second_input', 'Second Operand value is not valid'],
            ['operator', 'Please select Operator for your calculation'],
        ];
    }
}
