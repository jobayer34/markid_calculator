<?php

declare(strict_types=1);

namespace App\Tests\Unit\Services\Calculators;

use App\Services\Calculators\SummationCalculator;
use PHPUnit\Framework\TestCase;

class SummationCalculatorTest extends TestCase
{

    /**
     * @var SummationCalculator
     */
    private $summationCalculator;

    public function setUp(): void
    {
        $this->summationCalculator = new SummationCalculator();
    }

    public function testSupports()
    {
        $result = $this->summationCalculator->supports('+');
        $this->assertSame(true, $result);
    }

    /**
     * @dataProvider supportsValueProvider
     */
    public function testWrongSupportsValue($method, $expected)
    {
        $result = $this->summationCalculator->supports($method);
        $this->assertSame($expected, $result);
    }

    /**
     * @return array[]
     */
    public function supportsValueProvider(): array
    {
        return [
            ['/', false],
            ['*', false],
            ['-', false],
            ['+', true],
            ['@', false],
        ];
    }


    /**
     * @dataProvider resultValueProviderForCorrectData
     */
    public function testPopulatedResultForCorrectData($expected, $value1, $value2)
    {
        $result = $this->summationCalculator->getResult($value1, $value2);
        $this->assertSame($expected, $result);
    }

    /**
     * @return array[]
     */
    public function resultValueProviderForCorrectData(): array
    {
        return [
            [4, 2, 2],
            [6 , 4, 2],
            [6.0, 4.0, 2.0],
            [-2, -4, 2],
            [2, 0, 2]
        ];
    }
}
