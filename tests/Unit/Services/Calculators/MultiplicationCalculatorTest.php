<?php

declare(strict_types=1);

namespace App\Tests\Unit\Services\Calculators;

use App\Services\Calculators\MultiplicationCalculator;
use PHPUnit\Framework\TestCase;

class MultiplicationCalculatorTest extends TestCase
{
    /**
     * @var MultiplicationCalculator
     */
    private $multiplicationCalculator;

    public function setUp(): void
    {
        $this->multiplicationCalculator = new MultiplicationCalculator();
    }

    public function testSupports()
    {
        $result = $this->multiplicationCalculator->supports('*');
        $this->assertSame(true, $result);
    }

    /**
     * @dataProvider supportsValueProvider
     */
    public function testWrongSupportsValue($method, $expected)
    {
        $result = $this->multiplicationCalculator->supports($method);
        $this->assertSame($expected, $result);
    }

    /**
     * @return array[]
     */
    public function supportsValueProvider(): array
    {
        return [
            ['*', true],
            ['/', false],
            ['-', false],
            ['+', false],
            ['@', false],
        ];
    }


    /**
     * @dataProvider resultValueProviderForCorrectData
     */
    public function testPopulatedResultForCorrectData($expected, $value1, $value2)
    {
        $result = $this->multiplicationCalculator->getResult($value1, $value2);
        $this->assertSame($expected, $result);
    }

    /**
     * @return array[]
     */
    public function resultValueProviderForCorrectData(): array
    {
        return [
            [4, 2, 2],
            [8 , 4, 2],
            [8.0, 4.0, 2.0],
            [-8, -4, 2],
            [0, 0, 2]
        ];
    }


}
