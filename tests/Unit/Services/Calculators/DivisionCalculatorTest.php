<?php

declare(strict_types=1);

namespace App\Tests\Unit\Services\Calculators;

use App\Services\Calculators\DivisionCalculator;
use PHPUnit\Framework\TestCase;

class DivisionCalculatorTest extends TestCase
{

    /**
     * @var DivisionCalculator
     */
    private $divisionCalculator;

    public function setUp(): void
    {
        $this->divisionCalculator = new DivisionCalculator();
    }

    public function testSupports()
    {
        $result = $this->divisionCalculator->supports('/');
        $this->assertSame(true, $result);
    }

    /**
     * @dataProvider supportsValueProvider
     */
    public function testWrongSupportsValue($method, $expected)
    {
        $result = $this->divisionCalculator->supports($method);
        $this->assertSame($expected, $result);
    }

    /**
     * @return array[]
     */
    public function supportsValueProvider(): array
    {
        return [
            ['/', true],
            ['*', false],
            ['-', false],
            ['+', false],
            ['@', false],
        ];
    }


    /**
     * @dataProvider resultValueProviderForCorrectData
     */
    public function testPopulatedResultForCorrectData($expected, $value1, $value2)
    {
        $result = $this->divisionCalculator->getResult($value1, $value2);
        $this->assertSame($expected, $result);
    }

    /**
     * @return array[]
     */
    public function resultValueProviderForCorrectData(): array
    {
        return [
            [1, 2, 2],
            [2 , 4, 2],
            [2.0, 4.0, 2.0],
            [-2, -4, 2],
            [0, 0, 2]
        ];
    }

    public function testResultForWrongData()
    {
        $result = $this->divisionCalculator->getResult(4, 0);
        $this->assertSame("Second operand can't be zero!", $result);
    }
}
