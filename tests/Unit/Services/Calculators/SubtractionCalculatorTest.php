<?php

declare(strict_types=1);

namespace App\Tests\Unit\Services\Calculators;

use App\Services\Calculators\SubtractionCalculator;
use PHPUnit\Framework\TestCase;

class SubtractionCalculatorTest extends TestCase
{

    /**
     * @var SubtractionCalculator
     */
    private $subtractionCalculator;

    public function setUp(): void
    {
        $this->subtractionCalculator = new SubtractionCalculator();
    }

    public function testSupports()
    {
        $result = $this->subtractionCalculator->supports('-');
        $this->assertSame(true, $result);
    }

    /**
     * @dataProvider supportsValueProvider
     */
    public function testWrongSupportsValue($method, $expected)
    {
        $result = $this->subtractionCalculator->supports($method);
        $this->assertSame($expected, $result);
    }

    /**
     * @return array[]
     */
    public function supportsValueProvider(): array
    {
        return [
            ['/', false],
            ['*', false],
            ['-', true],
            ['+', false],
            ['@', false],
        ];
    }


    /**
     * @dataProvider resultValueProviderForCorrectData
     */
    public function testPopulatedResultForCorrectData($expected, $value1, $value2)
    {
        $result = $this->subtractionCalculator->getResult($value1, $value2);
        $this->assertSame($expected, $result);
    }

    /**
     * @return array[]
     */
    public function resultValueProviderForCorrectData(): array
    {
        return [
            [0, 2, 2],
            [2 , 4, 2],
            [2.0, 4.0, 2.0],
            [-6, -4, 2],
            [-2, 0, 2]
        ];
    }

}
