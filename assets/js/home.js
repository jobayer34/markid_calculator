$(document).on("click", "#calculatorBtn", (e)=>{
    e.preventDefault();

    let $form = $("#calculatorForm");
    let actionUrl = document.getElementById('calculatorForm').getAttribute('action');

    $("#fullPageLoader1").show();

    $.ajax({
        url: actionUrl,
        type: "POST",
        data: $form.serializeArray(),
        success: function(result){

            let errorBlock = $("#errorBlock");
            let successBlock = $("#successBlock");
            errorBlock.html("");
            successBlock.html("");

            if(result['status']) {
                let success = '<div class="text-success">'+ result['output'] +'</div>';
                successBlock.html(success);
            }
            else if(result['status'] === 0){

                let errors = "";
                $.each(result['errors'], (i, error) =>{
                    errors += '<li>' + error + '</li>';
                });

                errorBlock.html('<ul class="text-danger">' + errors + '</ul>');
            }else{
                errorBlock.html('<p class="text-danger">Something went wrong, please reload your page & try again</p>');
            }

            $("#fullPageLoader1").hide();
        },
        error: function(jqXHR) {
            $("#fullPageLoader1").hide();
            console.log('error found ', jqXHR.responseText);
        }
    })
})